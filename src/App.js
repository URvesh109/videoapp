import React, {Component} from 'react';
import {StatusBar} from 'react-native';
import reducers from './reducers';
import {createStore, applyMiddleware, compose} from 'redux';
import {Provider} from 'react-redux';
import Router from './Router';
import Thunk from 'redux-thunk';

StatusBar.setBackgroundColor('black');

if (__DEV__) {
    const xhr = global.originalXMLHttpRequest ? global.originalXMLHttpRequest : global.XMLHttpRequest;

    global.XMLHttpRequest = xhr;
    global.FormData = global.originalFormData ? global.originalFormData : global.FormData;
}

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const store = createStore(reducers, {}, composeEnhancers(applyMiddleware(Thunk)));

/**
 * App component
 */
class App extends Component {
    /**
     * @return {component}
     */
    render() {
        return (
            <Provider store={store}>
                <Router />
            </Provider>
        );
    }
}

export default App;

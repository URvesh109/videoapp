import * as types from '../action-types';
import API, {urls} from '../Api';

export const fetchVideoList = (loadMoreStatus) => async (dispatch) => {
    try {
        if (!loadMoreStatus) {
            dispatch({type: types.VIDEO_LIST_FETCHING_START});
        }
        const resp = await API.get(urls.videoList);
        const {status, data} = resp;
        let tempVidoes = Array.from(data.videos, (item) => ({...item, selected: false}));
        if (status == 200) {
            dispatch({
                type: types.VIDEO_LIST_FETCHING_SUCCESS,
                payload: {videos: tempVidoes},
            });
            return Promise.resolve('Success');
        }
        dispatch({
            type: types.VIDEO_LIST_FETCHING_ERROR,
            payload: {error: 'Something went wrong'},
        });
        return Promise.reject('message');
    } catch (e) {
        dispatch({
            type: types.VIDEO_LIST_FETCHING_ERROR,
            payload: {error: 'Please check your network connection.'},
        });
        return Promise.reject(String(e));
    }
};

export const loadMoreVideos = () => (dispatch, getState) => {
    const {list, mainList} = getState().video;
    dispatch({
        type: types.LOAD_MORE,
        payload: {videos: [...list, ...mainList]},
    });
};

export const onLikeBtnPress = (list) => (dispatch) => {
    dispatch({
        type: types.ON_LIKE_PRESS,
        payload: {videos: [...list]},
    });
};

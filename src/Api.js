import axios from 'axios';

const API = axios.create({
    baseURL: 'https://private-c31a5-task27.apiary-mock.com/',
});

export const urls = {
    videoList: 'videos',
};

export default API;

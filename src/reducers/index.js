import {combineReducers} from 'redux';
import video from './videoList';

export default combineReducers({
    video,
});

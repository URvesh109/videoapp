import * as types from '../action-types';

const INITIAL_STATE = {
    loading: true,
    list: [],
    mainList: [],
    error: '',
};

export default (state = INITIAL_STATE, action) => {
    const {type, payload} = action;
    switch (type) {
    case types.VIDEO_LIST_FETCHING_START:
        return {...INITIAL_STATE};
    case types.VIDEO_LIST_FETCHING_SUCCESS:
        return {...INITIAL_STATE, loading: false, list: payload.videos, mainList: payload.videos};
    case types.VIDEO_LIST_FETCHING_ERROR:
        return {...INITIAL_STATE, loading: false, error: 'Somthing went wrong'};
    case types.LOAD_MORE:
        return {...state, list: payload.videos};
    case types.ON_LIKE_PRESS:
        return {...state, list: payload.videos};
    default:
        return state;
    }
};

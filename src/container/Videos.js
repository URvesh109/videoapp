import React, {Component} from 'react';
import {View, Text, StyleSheet, ActivityIndicator, TouchableOpacity, Image, Dimensions, FlatList, RefreshControl, Linking} from 'react-native';
import {connect} from 'react-redux';
import {fetchVideoList, onLikeBtnPress, loadMoreVideos} from '../actions';
import PropTypes from 'prop-types';
import {Images} from '../assets';
import Icons from 'react-native-vector-icons/FontAwesome';
import {music} from '../Router';
const {width, heigth} = Dimensions.get('screen');

/**
 * Videos Component
 */
class Videos extends Component {
    static propTypes = {
        fetchVideoList: PropTypes.func,
        loadMoreVideos: PropTypes.func,
        pullToRefreshFunction: PropTypes.func,
        onLikeBtnPress: PropTypes.func,
        loading: PropTypes.bool,
        videoList: PropTypes.array,
        error: PropTypes.string,
        navigation: PropTypes.object,
    };

    /**
     * Initialize component
     * @param {object} props
     */
    constructor(props) {
        super(props);
        this.onTryAgainPress = this.onTryAgainPress.bind(this);
        this.onWhatsAppPress = this.onWhatsAppPress.bind(this);
        this.emptyComponent = this.emptyComponent.bind(this);
        this.onLikePress = this.onLikePress.bind(this);
        this.renderList = this.renderList.bind(this);
        this.pullToRefresh = this.pullToRefresh.bind(this);
        this.loadMoreContent = this.loadMoreContent.bind(this);
        this.onVideoTap = this.onVideoTap.bind(this);
        this.state = {
            selectedIndex: '',
            refresher: false,
            loadMore: false,
        };
    }

    /**
     * Lifecycle method
     */
    async componentDidMount() {
        try {
            await this.props.fetchVideoList(false);
        } catch (e) {
            console.log(e);
        }
    }

    /**
     * Load Initial data
     */
    async pullToRefresh() {
        try {
            this.setState({refresher: true});
            await this.props.fetchVideoList(true);
            this.setState({
                refresher: false,
            });
        } catch (e) {
            this.setState({refresher: false});
            console.log(e);
        }
    }

    /**
     * LoadMore
     */
    loadMoreContent() {
        const {loadMore} = this.state;
        if (loadMore) return;
        this.setState({loadMore: true});
        setTimeout(() => {
            this.props.loadMoreVideos();
            this.setState({
                loadMore: false,
            });
        }, 2000);
    }

    /**
     * On like show gif
     * @return {func}
     * @param {object} item
     * @param {number} index
     */
    onLikePress(item, index) {
        return () => {
            const {videoList} = this.props;
            const tempList = Array.from(videoList, (item, i) => {
                if (index == i) return {...item, selected: !item.selected};
                return item;
            });

            if (!item.selected) {
                this.setState({selectedIndex: String(index)});
                music.setVolume(0.9);
                music.play((success) => {
                    setTimeout(() => {
                        this.setState({selectedIndex: ''});
                    }, 3000);
                });
            }
            this.props.onLikeBtnPress(tempList);
        };
    }

    /**
     * If any issue occured while calling api
     */
    async onTryAgainPress() {
        try {
            await this.props.fetchVideoList();
        } catch (e) {
            console.log(e);
        }
    }

    /**
     * Navigate to play screen
     * @param {object} item
     * @return {function}
     */
    onVideoTap(item) {
        return () => {
            this.props.navigation.navigate('Play', {item});
        };
    }

    /**
     * Render empty component
     * @return {Component}
     */
    emptyComponent() {
        return (
            <View style={{height: heigth, marginTop: 100}}>
                <Text>No video is available</Text>
            </View>
        );
    }

    /**
     * Share url on whats app
     * @return {func}
     * @param {object} item
     */
    onWhatsAppPress(item) {
        return () => {
            let url = `whatsapp://send?text=${item}`;
            Linking.openURL(url);
            console.log('From whatsapp fresh');
        };
    }

    /**
     * Render list of videos
     * @param {object} item
     * @param {number} index
     * @return {Component}
     */
    renderList({item, index}) {
        return (
            <View>
                <View style={{paddingVertical: 5}}>
                    <Text style={{fontSize: 18}}>{item.title}</Text>
                </View>
                <TouchableOpacity activeOpacity={0.8} style={styles.imageStyle} onPress={this.onVideoTap(item)}>
                    <Image source={{uri: item.thumbnail_url}} style={styles.imageStyle} />
                    <View style={styles.playButtonStyle}>
                        <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
                            <View style={styles.playButtonCircle}>
                                <Icons size={40} name="play" color="white" />
                            </View>
                        </View>
                    </View>
                </TouchableOpacity>
                <View style={{alignItems: 'center', marginVertical: 5}}>
                    <View style={{width: width / 2, flexDirection: 'row', justifyContent: 'space-around'}}>
                        <TouchableOpacity activeOpacity={0.8} onPress={this.onWhatsAppPress(item.video_url)}>
                            <Icons size={30} name="whatsapp" color="#25d366" />
                        </TouchableOpacity>
                        {this.state.selectedIndex == String(index) ? (
                            <Image source={Images.like} resizeMode="cover" style={{height: 30, width: 30}} />
                        ) : (
                            <TouchableOpacity activeOpacity={0.8} onPress={this.onLikePress(item, index)}>
                                <Icons size={30} name={item.selected ? 'heart' : 'heart-o'} color="#e31b23" />
                            </TouchableOpacity>
                        )}
                    </View>
                </View>
            </View>
        );
    }

    /**
     * @return {COmponent}
     */
    render() {
        const {error, loading, videoList} = this.props;
        const {refresher} = this.state;
        if (loading && !refresher) {
            return (
                <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
                    <ActivityIndicator color="black" />
                </View>
            );
        }

        if (error) {
            return (
                <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
                    <Text>Please check your network connection</Text>
                    <View style={{marginTop: 10}}>
                        <TouchableOpacity activeOpacity={0.8} style={styles.tryAgainBtnStyle} onPress={this.onTryAgainPress}>
                            <Text>Try Again</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            );
        }
        return (
            <View style={styles.container}>
                <FlatList
                    showsVerticalScrollIndicator={false}
                    data={videoList}
                    renderItem={this.renderList}
                    ListEmptyComponent={this.emptyComponent}
                    keyExtractor={(item, index) => `My key is${index}`}
                    // extraData={this.props}
                    refreshControl={<RefreshControl refreshing={refresher} onRefresh={this.pullToRefresh} />}
                    onEndReached={this.loadMoreContent}
                    onEndReachedThreshold={0.5}
                    ListFooterComponent={
                        this.state.loadMore ? (
                            <View style={{paddingVertical: 40}}>
                                <ActivityIndicator color="black" />
                            </View>
                        ) : null
                    }
                />
            </View>
        );
    }
}

const mapStatesToProps = ({video}) => {
    return {
        loading: video.loading,
        videoList: video.list,
        error: video.error,
    };
};

export default connect(
    mapStatesToProps,
    {fetchVideoList, loadMoreVideos, onLikeBtnPress}
)(Videos);

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        backgroundColor: 'white',
        paddingTop: 5,
    },
    tryAgainBtnStyle: {
        padding: 10,
        borderWidth: 1,
        borderColor: 'black',
        borderRadius: 10,
    },
    imageStyle: {
        width: width - 20,
        height: 230,
        borderRadius: 10,
    },
    playButtonStyle: {
        width: '100%',
        height: '100%',
        position: 'absolute',
        backgroundColor: 'rgba(0,0,0,0.5)',
        borderRadius: 10,
    },
    playButtonCircle: {
        height: 60,
        width: 60,
        borderRadius: 50,
        borderWidth: 1,
        borderColor: 'white',
        justifyContent: 'center',
        alignItems: 'center',
        paddingLeft: 10,
    },
});

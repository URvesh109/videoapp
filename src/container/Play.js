import React, {Component} from 'react';
import Orientation from 'react-native-orientation';
import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import Video from 'react-native-video';
import Icons from 'react-native-vector-icons/FontAwesome';
import PropTypes from 'prop-types';

/**
 * Play Compoenent
 */
export default class Play extends Component {
    state = {
        rate: 1,
        volume: 1,
        muted: false,
        paused: true,
        resizeMode: 'cover',
        duration: 0.0,
        currentTime: 0.0,
        hideBtn: false,
    };

    static propTypes = {
        navigation: PropTypes.object,
    };

    /**
     * Lifecycle method
     */
    componentDidMount() {
        Orientation.lockToLandscape();
    }

    /**
     * Lifecycle method
     */
    componentWillUnmount() {
        Orientation.lockToPortrait();
    }

    onLoad = (data) => {
        this.setState({duration: data.duration});
    };

    onProgress = (data) => {
        this.setState({currentTime: data.currentTime, hideBtn: true});
    };

    onEnd = () => {
        this.setState({paused: true, hideBtn: false});
        this.video.seek(0);
    };

    onAudioBecomingNoisy = () => {
        this.setState({paused: true});
    };

    onAudioFocusChanged = (event: { hasAudioFocus: boolean }) => {
        this.setState({paused: !event.hasAudioFocus});
    };

    /**
     * Calculate percentage
     * @return {number}
     */
    getCurrentTimePercentage() {
        if (this.state.currentTime > 0) {
            return parseFloat(this.state.currentTime) / parseFloat(this.state.duration);
        }
        return 0;
    }

    /**
     * @return {Component}
     */
    render() {
        const flexCompleted = this.state.paused ? 0 : 1 + this.getCurrentTimePercentage() * 100;
        const flexRemaining = (1 - this.getCurrentTimePercentage()) * 100;
        const {
            item: {video_url: videoUrl},
        } = this.props.navigation.state.params;
        return (
            <View style={styles.container}>
                <TouchableOpacity style={styles.fullScreen} onPress={() => this.setState({paused: !this.state.paused})}>
                    <Video
                        ref={(ref) => {
                            this.video = ref;
                        }}
                        source={{uri: videoUrl}}
                        style={styles.fullScreen}
                        rate={this.state.rate}
                        paused={this.state.paused}
                        volume={this.state.volume}
                        muted={this.state.muted}
                        resizeMode={this.state.resizeMode}
                        onLoad={this.onLoad}
                        onProgress={this.onProgress}
                        repeat={false}
                    />
                    {!this.state.hideBtn ? (
                        <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
                            <Icons size={30} name={this.state.paused ? 'play' : 'pause'} color="white" />
                        </View>
                    ) : null}
                </TouchableOpacity>
                <View style={styles.controls}>
                    <View style={styles.progress}>
                        <Icons size={30} name="play" color="white" />
                        <View style={[styles.innerProgressCompleted, {flex: flexCompleted}]} />
                        <Icons size={20} name="circle" color="white" />
                        <View style={[styles.innerProgressRemaining, {flex: flexRemaining}]} />
                    </View>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'black',
    },
    fullScreen: {
        position: 'absolute',
        top: 0,
        left: 0,
        bottom: 0,
        right: 0,
    },
    controls: {
        backgroundColor: 'transparent',
        borderRadius: 5,
        position: 'absolute',
        bottom: 20,
        left: 20,
        right: 20,
    },
    progress: {
        flex: 1,
        flexDirection: 'row',
        borderRadius: 3,
        // height: 40,
        alignItems: 'center',
        overflow: 'hidden',
    },
    innerProgressCompleted: {
        height: 10,
        backgroundColor: 'white',
    },
    innerProgressRemaining: {
        height: 10,
        backgroundColor: '#2C2C2C',
    },
});

import {createStackNavigator} from 'react-navigation';
import Sound from 'react-native-sound';
import Videos from './container/Videos';
import Play from './container/Play';

export const music = new Sound('music.mp3', Sound.MAIN_BUNDLE, (error) => {
    console.log('Checking error is ', error);
});

const rootNav = createStackNavigator({
    Videos: {
        screen: Videos,
        navigationOptions: {
            title: 'Videos',
            header: null,
        },
    },
    Play: {
        screen: Play,
        navigationOptions: {
            title: 'Play',
            header: null,
        },
    },
});

export default rootNav;
